﻿using Xamarin.Forms.Xaml;
using Xamarin.Forms;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Fredoka-Regular.ttf", Alias = "Fredoka")]
[assembly: ExportFont("BalooThambi2-Regular.ttf", Alias = "BalooThambi")] 